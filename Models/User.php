<?php

use Intervention\Image\ImageManagerStatic as Image;

class User extends Model
{

    protected $table = 'users';

    /**
     * @Type varchar(255)
     */
    protected $first_name;

    /**
     * @Type varchar(255)
     */
    protected $suffix;

    /**
     * @Type varchar(255)
     */
    protected $last_name;

    /**
     * @Type varchar(255)
     */
    protected $username;

    /**
     * @Type varchar(255)
     */
    protected $password;

    /**
     * @Type varchar(255)
     */
    protected $adres;

    /**
     * @Type varchar(10)
     */
    protected $date_of_birth;

    /**
     * @Type varchar(45)
     */
    protected $role;

    /**
     * @@Type boolean
     */
    protected $active = true;


    public function __construct()
    {

    }


    public function getFullName()
    {
        $space = '';
        if ($this->suffix){
            $space = ' ';
        }
        return $this->first_name . " " . $this->suffix . $space . $this->last_name;
    }


    private function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);
    }


    private function checkPassword($password)
    {
        return password_verify($password, $this->password);
    }


    public static function generateSalt()
    {
        return uniqid();
    }


    protected static function newModel($obj)
    {

        $username = $obj->username;

        $existing = User::findBy('username', $username);
        if(count($existing) > 0) return false;

        //Check if user is valid
        return true;

    }


    public static function register($form)
    {
        if($form['password'] !== $form['repeat']) App::addError("passwords do not match");
        if(strlen($form['password']) < 8) App::addError("password is too short");

        if(isset($_SESSION['errors']) && count($_SESSION['errors'])) {
            return false;
        }

        $user = new User();
        $user->first_name = $form['first_name'];
        $user->suffix = $form['suffix'];
        $user->last_name = $form['last_name'];
        $user->username = $form['username'];
        $user->setPassword($form['password']);
        $user->adres = $form['adres'];
        $user->date_of_birth = $form['date_of_birth'];
        $user->role = 'user';
        $user->save();
        if($user->getId()) {
            App::setLoggedInUser($user);
            return $user;
        } else {
            return false;
        }
    }


    public static function login($form)
    {
        $username = $form['username'];
        $password = $form['password'];
        $users = self::findBy('username', $username);
        if (count($users) > 0) {
            $user = $users[0];
            if ($user->checkPassword($password)) {
                App::setLoggedInUser($user);
                return $user;
            }
        }
        App::addError("Combination does not exist");
        return false;
    }


    public static function updateUser($form)
    {
        $user = self::findById(App::$user->id);
        $user->first_name = $form['first_name'];
        $user->last_name = $form['last_name'];

        $user->save();
    }


    public static function loginForm()
    {
        $form = new Form();

        $form->addField((new FormField("username"))
            ->placeholder("username")
            ->required());

        $form->addField((new FormField("password"))
            ->type("password")
            ->placeholder("Password")
            ->required());

        return $form->getHTML();
    }


    public static function registerForm()
    {
        $form = new Form();

        $form->addField((new FormField("first_name"))
            ->placeholder("First name")
            ->required());

        $form->addField((new FormField("suffix"))
            ->placeholder("Suffix"));

        $form->addField((new FormField("last_name"))
            ->placeholder("Last name")
            ->required());

        $form->addField((new FormField("username"))
            ->placeholder("Username")
            ->required());

        $form->addField((new FormField("password"))
            ->type("password")
            ->placeholder("Password")
            ->required());

        $form->addField((new FormField("repeat"))
            ->type("password")
            ->placeholder("Password repeat")
            ->required());


        $form->addField((new FormField("adres"))
            ->placeholder("Adres")
            ->required());


        $form->addField((new FormField("date_of_birth"))
            ->type("date")
            ->placeholder("Date of birth")
            ->required());

        return $form->getHTML();
    }


    public static function editUserForm()
    {
        $user = User::findById(App::$user->id);

        $form = new Form();

        $form->addField((new FormField("username"))
            ->type("username")
            ->placeholder("username")
            ->value($user->username)
            ->required());

        $form->addField((new FormField("image"))
            ->type("file")
            ->placeholder("Image")
            ->value($user->image));

        $form->addField((new FormField("first_name"))
            ->placeholder("First name")
            ->value($user->first_name)
            ->required());

        $form->addField((new FormField("last_name"))
            ->placeholder("Last name")
            ->value($user->last_name)
            ->required());

        return $form->getHTML();
    }

}
