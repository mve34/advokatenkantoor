<?php

use Intervention\Image\ImageManagerStatic as Image;

class Article extends Model
{

    protected $table = 'Articles';

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type varchar(255)
     */
    protected $description;

    /**
     * @Type boolean
     */
    protected $public;

    public function __construct()
    {

    }


    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }





    protected static function newModel($obj)
    {
        return true;
    }


    public static function newArticle($form)
    {
        $article = new Article();
        $article->name = $form['name'];
        $article->description = $form['description'];
        $article->public = $form['public'];

        $article->save();
    }



    public static function updateArticle($form)
    {
        $article = self::findById($_GET['article_id']);
        $article->name = $form['name'];
        $article->description = $form['description'];

        $article->save();
    }


    public static function newArticleForm()
    {
        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("Article name")
            ->required());

        $form->addField((new FormField("description"))
            ->placeholder("Article description")
            ->required());

        $form->addField((new FormField("public"))
            ->type("select")
            ->values(["0" => "Not public", "1" => "Public"])
            ->placeholder("Public")
            ->required());


        return $form->getHTML();
    }


}
