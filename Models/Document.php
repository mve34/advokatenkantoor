<?php
/**
 * Created by PhpStorm.
 * User: 2039559
 * Date: 15-1-2019
 * Time: 13:51
 */

class Document extends Model
{
    protected $table = 'documents';

    /**
     * @Type varchar(255)
     */
    protected $name;

    /**
     * @Type varchar(255)
     */
    protected $file;

    /**
     * @Type int(255)
     */
    protected $article_id;

    public function __construct()
    {

    }


    public function getName()
    {
        return $this->name;
    }

    public function getFileName()
    {
        return $this->file;
    }

    public function getArticleId()
    {
        return $this->article_id;
    }


    protected static function newModel($obj)
    {
        return true;
    }


    public static function newDocument($form, $article_id)
    {
        $document = new Document();
//        var_dump($_FILES['file']['tmp_name']);
//        die();
        $document->name = $form['name'];
        $document->file = $_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['name'], Http::$dirroot.'public/documents/'.$document->file);
        $document->save($_FILES['file']['name'], Http::$dirroot.'public/documents/'.$document->file,100);
        $document->article_id = $article_id;

        $document->save();
    }


    public static function newDocumentForm()
    {
        $form = new Form();

        $form->addField((new FormField("name"))
            ->placeholder("Document name")
            ->required());

        $form->addField((new FormField("file"))
            ->type("file")
            ->placeholder("Select a file")
            ->required());

        return $form->getHTML();
    }

}