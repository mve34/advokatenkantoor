<?php
App::pageAuth([App::ROLE_ADMIN]);

$user = App::$user->findById($_GET['user_id']);

?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Account
        </div>
        <div class="card-body">
            <?= $user->getFullName(); ?>
            <br/>
            <?= $user->adres; ?>
            <br/>
            <?= $user->date_of_birth; ?>
            <br/>
            <?= $user->created_at; ?>
            <br/>
            <?= $user->updated_at; ?>
        </div>
    </div>
</div>

