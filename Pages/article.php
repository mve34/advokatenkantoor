<?php

App::pageAuth([App::ROLE_USER,App::ROLE_ADMIN], "login");

$article = Article::findById($_GET['article_id']);

if (!App::checkAuth(App::ROLE_ADMIN)) {
    if ($article->public == 0) {
        echo "<script> alert('you dont have acces to this article');</script>";
        App::redirect('home');
    }
}

?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Article: <?= $article->name . '<br/>'?>
        </div>
        <div class="card-body">
            <?= $article->description . "<br/><br/>" ?>

            <?php
            if (App::checkAuth(App::ROLE_ADMIN)) {
                echo "<a " . App::link('newDocument' . '&article_id=' . $article->id) . ">add new document</a>";
            }
            ?>

        </div>
    </div>
</div>