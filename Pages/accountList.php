<?php
App::pageAuth([App::ROLE_ADMIN]);

$users = User::get();


?>

<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            Account
        </div>
        <?php foreach ($users as $user) { ?>
        <div class="card-body">
            <a<?= App::link('adminAccountView&user_id='.$user->id);?>><?= "view the account of: " . $user->getFullName();?></a>
        </div>
        <?php } ?>
    </div>
</div>

