<?php

App::pageAuth([App::ROLE_ADMIN]);

$article = Article::findById($_GET['article_id']);

if (isset($_POST['name'])){
    Document::newDocument($_POST, $article->id);
}

?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            New document
        </div>
        <div class="card-body">
            <?= Document::newDocumentForm($article->id); ?>
        </div>
    </div>
</div>
