<?php

App::pageAuth([App::ROLE_ADMIN]);

if (isset($_POST['name'])){
    Article::newArticle($_POST);
}

?>
<div class="container">
    <div class="card card-model card-model-sm">
        <div class="card-header">
            New Article
        </div>
        <div class="card-body">
            <?= Article::newArticleForm(); ?>
        </div>
    </div>
</div>
